var parseGeoraster = require("georaster");
var GeoRasterLayer = require("georaster-layer-for-leaflet");

//Определяем карту, координаты центра и начальный масштаб
var mymap = L.map('map').setView([59.9386, 30.3141], 12);

var defaultLayer = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a rel="nofollow" href="http://osm.org/copyright">OpenStreetMap</a> contributors'
});

//Добавляем на нашу карту слой OpenStreetMap
defaultLayer.addTo(mymap)

var file1804 = "images/tif/1804.tif";
var file1939 = "images/tif/1939.tif";

var layerGroup = L.layerGroup();

fetch(file1804)
    .then(response => response.arrayBuffer())
    .then(arrayBuffer => {
        parseGeoraster(arrayBuffer)
            .then(georaster => {
                const layer1917 = new GeoRasterLayer({
                    georaster: georaster,
                    resolution: 200
                });

                fetch(file1939)
                    .then(response => response.arrayBuffer())
                    .then(arrayBuffer => {
                        parseGeoraster(arrayBuffer).then(georaster => {
                            const layer1939 = new GeoRasterLayer({
                                georaster: georaster,
                                resolution: 200
                            });

                            const layer2020 = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                                attribution: '&copy; <a rel="nofollow" href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                            })

                            const layers = {
                                1804: layer1917,
                                1939: layer1939,
                                2020: layer2020
                            };

                            L.control.layers(layers).addTo(mymap);
                            layer2020.addTo(mymap);
                        });
                    });
            });
    });


const marker = L.marker([59.948, 30.3166]).addTo(mymap);

marker.bindPopup("<p style='height: 210px; width: 200px'>Заячий Остров<br><img src=\"images/jpg/Zayachy_Island.jpg\" style='height: 200px; width: 200px'></p>").openPopup();

const alexanderGardenX1 = 30.30418;
const alexanderGardenY1 = 59.93438;
const alexanderGardenX2 = 30.30498;
const alexanderGardenY2 = 59.93663;

const alexanderGardenImg = "<p style='height: 210px; width: 200px'>Александровский Сад<img src=\"images/jpg/Alexander_Garden.jpg\" style='height: 200px; width: 200px'></p>"

const isaakievskiySoborX1 = 30.30595;
const isaakievskiySoborY1 = 59.93343;
const isaakievskiySoborX2 = 30.30642;
const isaakievskiySoborY2 = 59.93478;

const isaakievskiySoborImg = "<p style='height: 210px; width: 200px'>Исаакиевский Собор<img src=\"images/jpg/Isaakievskiy_Sobor.jpg\" style='height: 200px; width: 200px'></p>"

const kazanCathedralX1 = 30.32311;
const kazanCathedralY1 = 59.93393;
const kazanCathedralX2 = 30.32401;
const kazanCathedralY2 = 59.93523;

const kazanCathedralImg = "<p style='height: 210px; width: 200px'>Казанский Собор<img src=\"images/jpg/Kazan_Cathedral.jpg\" style='height: 200px; width: 200px'></p>"

const uronzeHorsemanX1 = 30.30211;
const uronzeHorsemanY1 = 59.93623;
const uronzeHorsemanX2 = 30.30221;
const uronzeHorsemanY2 = 59.93659;

const uronzeHorsemanImg = "<p style='height: 210px; width: 200px'>Медный Всадник<img src=\"images/jpg/Bronze_Horseman.jpg\" style='height: 200px; width: 200px'></p>"

const auroraX1 = 30.33706;
const auroraY1 = 59.95586;
const auroraX2 = 30.33856;
const auroraY2 = 59.95486;

const auroraImg = "<p style='height: 210px; width: 200px'>Крейсер Аврора<img src=\"images/jpg/Aurora.jpg\" style='height: 200px; width: 200px'></p>"

const popup = L.popup();

function onMapClick(e) {
    const x = e.latlng.lng;
    const y = e.latlng.lat;

    var content = "Координаты: [" + x.toFixed(5) + ", " + y.toFixed(5) + "]";

    if (x >= alexanderGardenX1 && x <= alexanderGardenX2 && y >= alexanderGardenY1 && y <= alexanderGardenY2) {
        content = alexanderGardenImg;

        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/info/1', false);
        xhr.send();
        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            $("#info").show();
            $("#info").html(xhr.response)
        }
    } else if (x >= isaakievskiySoborX1 && x <= isaakievskiySoborX2 && y >= isaakievskiySoborY1 && y <= isaakievskiySoborY2) {
        content = isaakievskiySoborImg;

        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/info/2', false);
        xhr.send();
        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            $("#info").show();
            $("#info").html(xhr.response)
        }
    } else if (x >= kazanCathedralX1 && x <= kazanCathedralX2 && y >= kazanCathedralY1 && y <= kazanCathedralY2) {
        content = kazanCathedralImg;

        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/info/3', false);
        xhr.send();
        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            $("#info").show();
            $("#info").html(xhr.response)
        }
    } else if (x >= uronzeHorsemanX1 && x <= uronzeHorsemanX2 && y >= uronzeHorsemanY1 && y <= uronzeHorsemanY2) {
        content = uronzeHorsemanImg;

        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/info/4', false);
        xhr.send();
        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            $("#info").show();
            $("#info").html(xhr.response)
        }
    } else if (x >= auroraX1 && x <= auroraX2 && y >= auroraY1 && y <= auroraY2) {
        content = auroraImg;

        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/info/5', false);
        xhr.send();
        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            $("#info").show();
            $("#info").html(xhr.response)
        }
    } else {
        $("#info").hide();
    }

    popup
        .setLatLng(e.latlng)
        .setContent(content)
        .openOn(mymap);
}

mymap.on('click', onMapClick);
